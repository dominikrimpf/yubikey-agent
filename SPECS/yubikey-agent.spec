%global debug_package %{nil}
%global goipath github.com/FiloSottile/yubikey-agent
Version: 0.1.5
%gometa

%global golicenses    LICENSE
%global godocs        *.md

Name:    yubikey-agent
Release: 1%{?dist}
Summary: yubikey-agent is a seamless ssh-agent for YubiKeys

License: BSD
URL:     %{gourl}
Source0: %{gosource}

Requires: pinentry pcsc-lite
BuildRequires: git pcsc-lite-devel systemd-rpm-macros

%description
yubikey-agent is a seamless ssh-agent for YubiKeys.

%prep
%goprep

%build
go build -o "%{gobuilddir}/bin/%{name}"

%install
install -m 0755 -vd                           %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/%{name} %{buildroot}%{_bindir}
install -m 0755 -vd                           %{buildroot}%{_userunitdir}
install -m 0644 -vp contrib/systemd/user/yubikey-agent.service %{buildroot}%{_userunitdir}

%files
%license %{golicenses}
%doc
%{_bindir}/%{name}
%{_userunitdir}/yubikey-agent.service

%post
%systemd_user_post %{name}.service

%preun
%systemd_user_preun %{name}.service

%changelog
* Thu Sep 02 2021 Dominik Rimpf <dev@drimpf.de>
- First SPEC release
